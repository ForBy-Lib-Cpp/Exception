//  ForBy LTD 2017
/// Exception

/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef FORBY_EXCEPTION_INCLUDED
#define FORBY_EXCEPTION_INCLUDED

#define _forby_exception_edition 0x0011

#include <stdexcept>
#include <string>
#include <cstring>

namespace ForBy
{

    enum Exception_level
    {
        Warning
    ,   Alert
    ,   Fail
    ,   Error
    ,   Fatal
    };

    // -----------------

    class Exception
    : virtual public std::exception
    {
        std::string _msg;

        const Exception_level ex;

    public:

        inline Exception_level const& level() const noexcept {
            return ex;
        }

        inline const char* what() const noexcept {
            return _msg.c_str() ;
        }


        // -------------------------------

        inline
        Exception( Exception_level _ex = Fail, int error_code = errno )
        :  ex(_ex)
        {
            _msg = strerror( error_code );

        } // C friendly exception

        inline
        Exception( const std::string& msg , Exception_level ex = Fail , bool catch_c_error = false )
        :   ex(ex)
        {
            _msg = msg;
            if( catch_c_error )
            {
                _msg += " ";
                _msg += strerror(errno);
            }
        }


        inline
        Exception( const Exception& _other)
        : ex( _other.level() )
        {
            this->_msg = _other.what();
        }

        inline
        Exception( Exception&& _other)
        : ex( _other.level() )
        {
            this->_msg = _other.what();
        }

    };

    template
    < typename Func
    , typename... Args
    , typename Return = std::result_of<Func>
    , typename F = std::decay_t<Func>
    >
    inline bool
    try_func(F& func, Args&& ...args )
    {
        try
        {
            func(std::forward<args>...);
        }
        catch( Exception& e){
            return false;
        }

        return true;
    }


} // namespace ForBy

#endif // FORBY_EXCEPTION_INCLUDED
